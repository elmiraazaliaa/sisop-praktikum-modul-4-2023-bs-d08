#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <limits.h>

#define LOG_FILE "/home/elmiraazalia/fs_module.log"

static FILE *log_file;

void log_event(const char *level, const char *cmd, const char *old_path, const char *new_path) {
    time_t now;
    struct tm *timeinfo;
    char timestamp[20];

    time(&now);
    timeinfo = localtime(&now);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", timeinfo);

    if (new_path != NULL) {
        fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, old_path);
        fprintf(log_file, "%s::%s::%s\n", level, timestamp, new_path);
    } else {
        fprintf(log_file, "%s::%s::%s\n", level, timestamp, old_path);
    }

    fflush(log_file);
}

static int fs_getattr(const char *path, struct stat *stbuf) {
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;

    stbuf->st_mode |= 0777;

    return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);
    if (dp == NULL) {
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0)) break;
    }

    closedir(dp);
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode) {
    int res;
    res = mkdir(path, mode);
    if (res == -1) {
        return -errno;
    }

    chmod(path, mode);

    log_event("REPORT", "MKDIR", path, NULL);

    return 0;
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char fullpath[PATH_MAX];
    snprintf(fullpath, PATH_MAX, "%s%s", (char *)fuse_get_context()->private_data, path);

    int res;
    res = creat(fullpath, mode);
    if (res == -1) {
        return -errno;
    }

    close(res);

    log_event("REPORT", "CREATE", path, NULL);

    return 0;
}

static int fs_rename(const char *oldpath, const char *newpath) {
    int res;
    res = rename(oldpath, newpath);
    if (res == -1) {
        return -errno;
    }

    log_event("REPORT", "RENAME", oldpath, newpath);

    return 0;
}

static int fs_rmdir(const char *path) {
    int res;
    res = rmdir(path);
    if (res == -1) {
        return -errno;
    }

    log_event("FLAG", "RMDIR", path, NULL);

    return 0;
}

static int fs_unlink(const char *path) {
    int res;
    res = unlink(path);
    if (res == -1) {
        return -errno;
    }

    log_event("FLAG", "UNLINK", path, NULL);

    return 0;
}

static struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .mkdir = fs_mkdir,
    .create = fs_create,
    .rename = fs_rename,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
};

int main(int argc, char *argv[]) {
    umask(0);

    log_file = fopen(LOG_FILE, "a");
    if (log_file == NULL) {
        perror("Cannot open log file");
        return 1;
    }

    return fuse_main(argc, argv, &fs_oper, NULL);
}
